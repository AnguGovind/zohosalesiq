
#import "RNZohoSalesIQ.h"
#import <Mobilisten/Mobilisten.h>

@implementation RNZohoSalesIQ

RCT_EXPORT_MODULE();

- (dispatch_queue_t)methodQueue
{
    return dispatch_get_main_queue();
}
RCT_EXPORT_METHOD(init:(NSString *)appKey accessKey:(NSString *)accessKey)
{
    printf("INITNCALLLED");
    [ZohoSalesIQ initWithAppKey:APP KEY accessKey:ACCESS KEY completion:^(BOOL completed) { }];
}
RCT_EXPORT_METHOD(setChatTitle : (NSString)chattitle){
    printf("setChatTitle CALLLED");
}
RCT_EXPORT_METHOD(setLanguage: (NSString)language_code){
    ZohoSalesIQ.Chat.setLanguage(language_code);
}
RCT_EXPORT_METHOD(setDepartment: (NSString)department){
    ZohoSalesIQ.Chat.setDepartment(department);
}
RCT_EXPORT_METHOD(setOperatorEmail: (NSString)email){
    ZohoSalesIQ.Chat.setAgentEmail(email);
}
RCT_EXPORT_METHOD(showOperatorImageinLauncher: (NSNumber)show){
    ZohoSalesIQ.Chat.setVisibility(.agentPhotoOnChatIcon, visible: show);
}
RCT_EXPORT_METHOD(showChat){
    //ZohoSalesIQ.Chat.show();
}
RCT_EXPORT_METHOD(showLauncher: (NSNumber)show){
    ZohoSalesIQ.showLiveChat(show);
}
RCT_EXPORT_METHOD(setVisitorName: (NSString)name){
    ZohoSalesIQ.Visitor.setName(name);
}
RCT_EXPORT_METHOD(setVisitorEmail: (NSString)email){
    ZohoSalesIQ.Visitor.setEmail(email);
}
RCT_EXPORT_METHOD(setVisitorContactNumber: (NSString)number){
    ZohoSalesIQ.Visitor.setContactNumber(number);
}
RCT_EXPORT_METHOD(visitorAddInfo: (NSString *)key value:(NSString *)value){
    ZohoSalesIQ.Visitor.addInfo(key, value: value);
}
RCT_EXPORT_METHOD(setQuestion: (NSString)question){
    ZohoSalesIQ.Visitor.setQuestion(question);
}
RCT_EXPORT_METHOD(startChat: (NSString)message){
    ZohoSalesIQ.Chat.startChat(question: message);
}
RCT_EXPORT_METHOD(setPageTitle: (NSString)pagetitle){
    ZohoSalesIQ.Tracking.setPageTitle(pagetitle);
}
RCT_EXPORT_METHOD(setCustomAction: (NSString)action_name){
    ZohoSalesIQ.Tracking.setCustomAction(action_string);
}
RCT_EXPORT_METHOD(setConversationPageTitle: (NSString)conv_page_title){

}

@end
